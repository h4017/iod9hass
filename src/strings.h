#define ON "ON"
#define OFF "OFF"

// void setupDisplay
#define SD01 "Setting up display"
#define SD02 "Display configured."

// void connectToWifi
#define CT01 "Connecting to Wi-Fi..."

// void connectToMqtt
#define CT02 "Connecting to MQTT..."

// void onWifiConnect( const WiFiEventStationModeGotIP &event )
#define WC01 "Connected to Wi-Fi."

// void onWifiDisconnect( const WiFiEventStationModeDisconnected &event )
#define WD01 "Disconnected from Wi-Fi."

// void onMqttConnect( bool sessionPresent )
#define MC01 "Connected to MQTT."
#define MC02 "Session present: "

// void onMqttDisconnect( AsyncMqttClientDisconnectReason reason )
#define MD01 "Disconnected from MQTT."
#define MD02 "Subscribe acknowledged."
#define MD03 "  packetId: "
#define MD04 "       qos: "

// void onMqttUnsubscribe( uint16_t packetId )
#define MU01 "Unsubscribe acknowledged."

// void onMqttPublish( uint16_t packetId )
#define MP01 "Publish acknowledged."

// int getState( String payload )
#define GS01 "Invalid state."

// void doLight( String payload )
#define DL01 "Light: "

// void doBrightness( String payload )
#define DB01 "Brightness: "

// void doHeat( String payload )
#define DH01 "Heater: "

// void onMqttMessage( char *topic, char *payload, ...
#define MM01 "Message acknowledged."
#define MM02 "    topic: "
#define MM03 "  payload: "
#define MM04 "     size: "

// void displayTemp()
#define DT01 "C "
#define DT02 "error"
// Create a copy of this file called "config.h"

#define WIFI_SSID "<your ssid goes here>"
#define WIFI_PASSWORD "<your password goes here>"

// Change the IP address and port below as per your MQTT server
#define MQTT_HOST IPAddress( 192, 168, 0, 1 )
#define MQTT_PORT 1883

#define MQTT_CHANNEL "<your mqtt channel>"

#define ONEWIREPIN 2
#define LIGHTPIN 16
#define HEATPIN 0

// Allows LIGHTPIN to use PWM to control the brightness of the light
#define BRIGHTNESS

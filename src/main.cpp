#include <AsyncMqttClient.h>
#include <DallasTemperature.h>
#include <ESP8266WiFi.h>
#include <GFX4dIoD9.h>
#include <OneWire.h>
#include <Ticker.h>

#define DEBUG

// This include file contains the stuff that I don't want publicly available
// and configs that may be changed for different modules/tanks
// Create a copy of the included config.template.h and edit as needed
#include <config.h>

#include <strings.h>

// MQTT channel for setting the time
#define MQTT_SUB_TIME  "home/time"

// MQTT channel for boot announcement
#define MQTT_PUB_BOOT  MQTT_CHANNEL "/boot"

// MQTT channels for temperature & heater
#define MQTT_PUB_TEMP  MQTT_CHANNEL "/temperature"
#define MQTT_SUB_HEAT  MQTT_CHANNEL "/heater/command"
#define MQTT_PUB_HEAT  MQTT_CHANNEL "/heater/state"

// MQTT channels for light
#define MQTT_SUB_LIGHT MQTT_CHANNEL "/light/command"
#define MQTT_PUB_LIGHT MQTT_CHANNEL "/light/state"
#define MQTT_SUB_LIGHTBRIGHT MQTT_CHANNEL "/light/brightness/command"
#define MQTT_PUB_LIGHTBRIGHT MQTT_CHANNEL "/light/brightness/state"

#define MAXTEMP 29 // This is a hard-coded safetly level at which the
                   // heater will be forced off, regardless of what
                   // instructions are sent via MQTT

// GPIO where the DS18B20 is connected
const int oneWireBus = ONEWIREPIN;
// Setup a oneWire instance to communicate with any OneWire devices
OneWire oneWire( oneWireBus );
// Pass our oneWire reference to Dallas Temperature sensor
DallasTemperature sensors( &oneWire );

// Sensor current temperature value
float temperature;

// The dallas library returns -127 when it can't read the sensor
// Do not report temperature in that case
const float dallasErrorTemperature = -127;

AsyncMqttClient mqttClient;
Ticker mqttReconnectTimer;

WiFiEventHandler wifiConnectHandler;
WiFiEventHandler wifiDisconnectHandler;
Ticker wifiReconnectTimer;

unsigned long mqttMillis    = 0; // Stores last time temperature was published
unsigned long displayMillis = 0; // Stores last time display was updated

const unsigned long mqttInterval    = 15 * 1000; // Interval at which to publish sensor readings
const unsigned long displayInterval = 1 * 1000; // Interval at which to update display

/* #region Display settings */
GFX4dIoD9 gfx = GFX4dIoD9();
const uint16 backgroundColour = BLACK;
const uint16 dayTimeColour    = SKYBLUE;
const uint16 nightTimeColour  = INDIGO;

const uint16 heatingColour    = DARKRED;
const uint16 tempOkColour     = DARKGREEN;
/* #endregion */

/* #region Relays */
const int lightPin = LIGHTPIN;
bool lightState = false;

#ifdef BRIGHTNESS
byte brightness = 128;
#endif

const int heatPin = HEATPIN;
bool heatState = false;
/* #endregion */

/* #region Time variables */
unsigned int month;
unsigned int day;
unsigned int hour;
unsigned int minute;
unsigned int second;
/* #endregion */

void setupDisplay() {
#ifdef DEBUG
  Serial.println( SD01 );
#endif

  gfx.begin();
  gfx.Cls();
  gfx.ScrollEnable( false );
  gfx.Orientation( LANDSCAPE );
  gfx.FillScreen( backgroundColour );

#ifdef DEBUG
  Serial.println( SD02 );
#endif
}

void connectToWifi() {
#ifdef DEBUG
  Serial.println(  );
#endif
  WiFi.begin( WIFI_SSID, WIFI_PASSWORD );
}

void connectToMqtt() {
#ifdef DEBUG
  Serial.println( CT02 );
#endif
  mqttClient.connect();
}

void onWifiConnect( const WiFiEventStationModeGotIP &event ) {
#ifdef DEBUG
  Serial.println( WC01 );
#endif
  connectToMqtt();
}

void onWifiDisconnect( const WiFiEventStationModeDisconnected &event ) {
#ifdef DEBUG
  Serial.println( WD01 );
#endif
  mqttReconnectTimer.detach(); // ensure we don't reconnect to MQTT while reconnecting to Wi-Fi
  wifiReconnectTimer.once( 2, connectToWifi );
}

void onMqttConnect( bool sessionPresent ) {
#ifdef DEBUG
  Serial.println( MC01 );
  Serial.print( MC02 );
  Serial.println( sessionPresent );
#endif

  mqttClient.subscribe( MQTT_SUB_LIGHT, 0 );
  mqttClient.subscribe( MQTT_SUB_HEAT, 0 );
  mqttClient.subscribe( MQTT_SUB_TIME, 0 );

#ifdef BRIGHTNESS
  mqttClient.subscribe( MQTT_SUB_LIGHTBRIGHT, 0 );
#endif

  if ( temperature > dallasErrorTemperature )
    mqttClient.publish( MQTT_PUB_TEMP, 1, true, String( temperature ).c_str() ); 

  mqttClient.publish( MQTT_PUB_HEAT, 1, true, heatState ? ON : OFF );
  mqttClient.publish( MQTT_PUB_LIGHT, 1, true, lightState ? ON : OFF );
  mqttClient.publish( MQTT_PUB_BOOT, 1, true, ( WiFi.localIP().toString() + ", " + WiFi.macAddress() ).c_str() );

#ifdef BRIGHTNESS
  mqttClient.publish( MQTT_PUB_LIGHTBRIGHT, 1, true, String( brightness ).c_str() );
#endif
}

void onMqttDisconnect( AsyncMqttClientDisconnectReason reason ) {
#ifdef DEBUG
  Serial.println( MD01 );
#endif

  if ( WiFi.isConnected() )
  {
    mqttReconnectTimer.once( 2, connectToMqtt );
  }
}

void onMqttSubscribe( uint16_t packetId, uint8_t qos ) {
#ifdef DEBUG
  Serial.println( MD02 );
  Serial.print( MD03 );
  Serial.println( packetId );
  Serial.print( MD04 );
  Serial.println( qos );
#endif
}

void onMqttUnsubscribe( uint16_t packetId ) {
#ifdef DEBUG
  Serial.println( MU01 );
  Serial.print( MD03 );
  Serial.println( packetId );
#endif
}

void onMqttPublish( uint16_t packetId ) {
#ifdef DEBUG
  Serial.println(  );
  Serial.print( MD03 );
  Serial.println( packetId );
#endif
}

#ifdef BRIGHTNESS
boolean isValidNumber( String str ) {
  for ( byte i = 0; i < str.length(); i++ )
  {
    if ( !isDigit( str.charAt( i ) ) )
      return false;
  }
  return true;
}
#endif

int getState( String payload ) {
  if ( payload.compareTo( ON ) == 0 ) {
    return 1;
  } else if ( payload.compareTo( OFF ) == 0 ) {
    return 0;
#ifdef BRIGHTNESS
  } else if ( isValidNumber( payload ) ) {
    return payload.toInt();
#endif
  } else {
#ifdef DEBUG
    Serial.println( GS01 );
#endif
    return -1;
  }
}

void printDigits( int digits, char spacer ) {
  if ( digits < 10 )
    gfx.print( spacer );

  gfx.print( digits );
}

void displayTime() {
  gfx.FontStyle( DOTMATRIXFADE );
  gfx.TextSize( 5 );
  gfx.TextColor( lightState ? dayTimeColour : nightTimeColour, backgroundColour );

  gfx.MoveTo( 6, 10 );
  printDigits( hour, ' ' );
  gfx.print( second % 2 == 0 ? ":" : " " );
  printDigits( minute, '0' );
}

void doTime( String payload ) {
  // Payload format: 2020-09-26 21:40:00.003343+02:00
  // 32 characters:  01234567890123456789012345678901
  month  = payload.substring(  5,  7 ).toInt();
  day    = payload.substring(  8, 10 ).toInt();
  hour   = payload.substring( 11, 13 ).toInt();
  minute = payload.substring( 14, 16 ).toInt();
  second = payload.substring( 17, 19 ).toInt();
}

void doLight( String payload ) {
  int state = getState( payload );
  if ( state == -1 )
    return;
  
  lightState = state == 1;

#ifdef DEBUG
  Serial.print( DL01 );
  Serial.println( payload );
#endif

#ifdef BRIGHTNESS
  analogWrite( lightPin, lightState * brightness );
#else
  digitalWrite( lightPin, lightState );
#endif
  if ( mqttClient.connected() )
    mqttClient.publish( MQTT_PUB_LIGHT, 1, true, payload.c_str() );
}

#ifdef BRIGHTNESS
void doBrightness( String payload ) {
  byte state = getState( payload );
  if ( state == -1 )
    return;
  
  brightness = state;

#ifdef DEBUG
  Serial.print( DB01 );
  Serial.println( payload );
#endif

  analogWrite( lightPin, lightState * brightness );

  if ( mqttClient.connected() )
    mqttClient.publish( MQTT_PUB_LIGHTBRIGHT, 1, true, String( brightness ).c_str() );
}
#endif

void doHeat( String payload ) {
  int state = getState( payload );
  if ( state == -1 )
    return;
  
  heatState = ( state == 1 ) && ( temperature < MAXTEMP );

#ifdef DEBUG
  Serial.print( DH01 );
  Serial.println( payload );
#endif
  digitalWrite( heatPin, heatState );
  if ( mqttClient.connected() )
    mqttClient.publish( MQTT_PUB_HEAT, 1, true, payload.c_str() );
}

void onMqttMessage( char *topic, char *payload, AsyncMqttClientMessageProperties properties, size_t len, size_t index, size_t total ) {
#ifdef DEBUG
  Serial.println( MM01 );
  Serial.print( MM02 );
  Serial.println( topic );
  Serial.print( MM03 );
  Serial.println( payload );
  Serial.print( MM04 );
  Serial.println( len );
#endif

  if ( String( topic ).compareTo( String( MQTT_SUB_TIME ) ) == 0 )
    doTime( String( payload ).substring( 0, (int)len) );

  if ( String( topic ).compareTo( String( MQTT_SUB_LIGHT ) ) == 0 )
    doLight( String( payload ).substring( 0, (int)len ) );

#ifdef BRIGHTNESS
  if ( String( topic ).compareTo( String( MQTT_SUB_LIGHTBRIGHT ) ) == 0 )
    doBrightness( String( payload ).substring( 0, (int)len ) );
#endif

  if ( String( topic ).compareTo( String( MQTT_SUB_HEAT ) ) == 0 )
    doHeat( String( payload ).substring( 0, (int)len ) );
}

void displayTemp() {
  gfx.FontStyle( SOLID );
  gfx.TextSize( 3 );
  gfx.TextColor( heatState ? heatingColour : tempOkColour, backgroundColour );
  gfx.MoveTo( 20, 50 );
  
  if ( temperature > dallasErrorTemperature ) {
    gfx.print( temperature );
    gfx.print( DT01 );
  } else {
    gfx.print( DT02 );
  }  
}

void setup() {
  sensors.begin();

#ifdef DEBUG
  Serial.begin( 9600 );
  Serial.println();
#endif

  setupDisplay();

  wifiConnectHandler = WiFi.onStationModeGotIP( onWifiConnect );
  wifiDisconnectHandler = WiFi.onStationModeDisconnected( onWifiDisconnect );

  mqttClient.onConnect( onMqttConnect );
  mqttClient.onDisconnect( onMqttDisconnect );
  mqttClient.onSubscribe( onMqttSubscribe );
  mqttClient.onUnsubscribe( onMqttUnsubscribe );
  mqttClient.onPublish( onMqttPublish );
  mqttClient.onMessage( onMqttMessage );
  mqttClient.setServer( MQTT_HOST, MQTT_PORT );

#ifdef MQTT_USER 
  mqttClient.setCredentials( MQTT_USER, MQTT_PASSWORD );
#endif

#ifndef BRIGHTNESS
  pinMode( lightPin, OUTPUT );
#endif
  pinMode( heatPin, OUTPUT );

  doLight( OFF );
  doHeat( ON );

  connectToWifi();
}

void loop() {
  unsigned long currentMillis = millis();

  if ( currentMillis - displayMillis >= displayInterval )
  {
    if ( ++second >= 60 ) {   // this is a very bad clock
      if ( ++minute >= 60 ) { // but no worries,
        if ( ++hour >= 24 )   // it gets updated regularly
          hour = 0;           // from the server, which
        minute = 0;           // synchronises with NTP
      }
      second = 0;
    }

    sensors.requestTemperatures();
    temperature = sensors.getTempCByIndex( 0 );
    
    displayTemp();
    displayTime();
    displayMillis = currentMillis;
  }

  if ( temperature >= MAXTEMP && heatState )
    doHeat( OFF );

  if ( currentMillis - mqttMillis >= mqttInterval ) {
    mqttMillis = currentMillis;

    if ( temperature > dallasErrorTemperature )
      mqttClient.publish( MQTT_PUB_TEMP, 1, true, String( temperature ).c_str() ); 

    mqttClient.publish( MQTT_PUB_LIGHT, 1, true, lightState ? ON : OFF );
    mqttClient.publish( MQTT_PUB_HEAT, 1, true, heatState ? ON : OFF );
  }
}
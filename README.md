# iod9hass
A project for using 4D Systems' IOD-09TH module with Home Assistant and MQTT to manage the temperature and lighting of my fish tanks.

## IoD module
https://4dsystems.com.au/iod-09th

## Features
* Time-based control of lighting
* Temperature control and monitoring

## Libraries used
* AsyncMqttClient https://github.com/marvinroger/async-mqtt-client.git
* DallasTemperature https://github.com/milesburton/Arduino-Temperature-Control-Library.git
* ESP8266WiFi https://github.com/platformio/platform-espressif8266
* GFX4dIoD9 https://github.com/4dsystems/GFX4DIoD9 (see build notes below) 
* OneWire https://github.com/PaulStoffregen/OneWire

Most of the code comes from the examples in these libraries.

## Build Notes
Use version 3.0 of the Espressif 8266 platform.
```
platform = espressif8266@3.0
```

## Security and config
Copy the config.template.h file to config.h and edit as needed. This file will not be committed to git.

## BUT!?
Why did 4D Systems only give this module 4Mbit (512kb) of storage? This SEVERELY limits the size of the application and what can be done.

One of the challenges that I encountered is that I could not include an NTP library. How I overcame this was to use MQTT creatively. When the module boots, it broadcasts its presence. Home Assistant will then broadcast the time, which the module listens for and sets its clock. It's hacky, but it works really well :)

You may also notice that the logic in the "Features" section is not in the code here... That's right! All of that logic is in Home Assistant. Size constraints.. sigh.

## Regions?
I'm using a region plugin for vscode...
https://marketplace.visualstudio.com/items?itemName=maptz.regionfolder

## Why didn't I use Tasmota/ESPEasy/ESPHome etc?
I looked at many different projects before deciding to code my own:
* https://tasmota.github.io/docs/
* https://www.letscontrolit.com/wiki/index.php/ESPEasy
* https://esphome.io/
* https://github.com/xoseperez/espurna/
None of them would fit on my 4Mbit (512kb) flash! 